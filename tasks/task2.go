package main

import (
	"fmt"
	"time"
)

func main() {
	dobStr := "25.02.2002" // Replace this date with your birthday
	givenDate, err := time.Parse("02.01.2006", dobStr)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s - Haftaning  %s", givenDate.Format("02-01-2006"), FindWeekday(givenDate) )
}

func FindWeekday(date time.Time) (weekday string) {
	//
	// WRITE YOUR CODE HERE
	weekday = date.Weekday().String()

	switch weekday {
	case "Monday":
		//  fmt.Println("Haftaning Dushanba kuniga to'g'ri keladi")
		return "Dushanba kuniga to'g'ri keladi"
	case "Tuesday": 
		return "Seshanba kuniga to'g'ri keladi"

	case "Wednesday":
		return "Chorshanba kuniga to'g'ri keladi"

	case "Thursday":
		return "Payshanba kuniga to'g'ri keladi"

	case "Friday":
		return "JUMA kuniga to'g'ri keladi"

	case "Saturday":
		return "Shanba kuniga to'g'ri keladi"

	case "Sunday":
		return "Yakshanba kuniga to'g'ri keladi"
	}
return weekday
}

