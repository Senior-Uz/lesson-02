package main

import "fmt"

func main() {
	DisplayNumberInReverseOrderWithDefer()
}

func DisplayNumberInReverseOrderWithDefer() (defered int ){
	for i := 0; i < 100; i++ {
		//
		// WRITE YOUR CODE HERE
		defer fmt.Println(i + 1)
	}
	  return

}
